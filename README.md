# Spring Boot Aws Sqs

This project is to connect aws sqs and then generate/publish messages to the queue 

## Getting started
Make sure to provide AWS SQS information in **application.properties** to connect required SQS queue.

You can also update the PORT in application.properties to change the port if required. 

To generate JAR file, please run "**mvn clean install**"


