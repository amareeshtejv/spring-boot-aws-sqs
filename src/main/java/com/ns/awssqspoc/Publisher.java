package com.ns.awssqspoc;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.UUID;

@Component
@Slf4j
public class Publisher {


    @Autowired
    private AmazonSQS amazonSQS;


    @Value("${cloud.aws.end-point.uri}")
    private String endpoint;

    @Value("${count}")
    private Integer count;

    @Value("${sendfile}")
    private Boolean sendfile;

    @Value("${filepath}")
    private String filepath;

    @Scheduled(fixedRate = 1)
    public void scheduleFixedRateTask() throws IOException
    {
        if(sendfile){
            sendMultipleFiles();
        } else
        {
            sendMultiple();
        }
    }
    public void sendMultiple() {
        SendMessageBatchRequestEntry[] dataArray = new SendMessageBatchRequestEntry[count];
        MessageRequest msg = new MessageRequest();
        for (int i = 0; i < count; i++) {
                    msg.setAccount_id(String.valueOf(i));
                    msg.setAccount_uuid("testing68983-" + i);
                    msg.setEvent_name("Wiki_page_created");
                    msg.setEvent_time(String.valueOf(Instant.now()));
                    msg.setIs_mock_event("true");
            dataArray[i] = new SendMessageBatchRequestEntry(UUID.randomUUID().toString(), new Gson().toJson(msg));
        }

        SendMessageBatchRequest sendBatchRequest =
                new SendMessageBatchRequest()
                        .withQueueUrl(endpoint)
                        .withEntries(dataArray);
            amazonSQS.sendMessageBatch(sendBatchRequest);
            amazonSQS.sendMessageBatch(sendBatchRequest);
            amazonSQS.sendMessageBatch(sendBatchRequest);

        log.info("Sending Message to SQS ");
    }
    public void sendMultipleFiles() throws IOException
    {
        SendMessageBatchRequestEntry[] dataArray1 = new SendMessageBatchRequestEntry[count];
        for (int j = 0; j < count; j++) {
            dataArray1[j] = new SendMessageBatchRequestEntry(UUID.randomUUID().toString(), Files.readString(Paths.get(filepath),
                                                                                                            StandardCharsets.US_ASCII));
        }

        SendMessageBatchRequest sendBatchRequest =
                new SendMessageBatchRequest()
                        .withQueueUrl(endpoint)
                        .withEntries(dataArray1);
        amazonSQS.sendMessageBatch(sendBatchRequest);
        amazonSQS.sendMessageBatch(sendBatchRequest);
        amazonSQS.sendMessageBatch(sendBatchRequest);

        log.info("Sending file from local to SQS ");
    }

}
