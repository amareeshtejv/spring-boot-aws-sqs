package com.ns.awssqspoc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageRequest
{
    private String account_id;
    private String account_uuid;
    private String event_name;
    private String event_time;
    private String is_mock_event;
}
